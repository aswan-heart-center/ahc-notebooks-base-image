# A docker image to run data science notebooks

Based on [jupyter/tensorflow-notebook](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-tensorflow-notebook)

## Supported Kernels:

- Python

## Python Packages:

pandas, numexpr, matplotlib, scipy, seaborn, scikit-learn, scikit-image, sympy, cython, patsy, statsmodel, cloudpickle, dill, numba, bokeh, sqlalchemy, hdf5, vincent, beautifulsoup, protobuf, xlrd, ipywidgets, Facets, plotly, plotly-express, cufflinks, pymongo, python-dotenv, tensorflow, keras,jupyter-contrib-nbextensions, qgrid, pandas-profiling, pytest, simplejson, streamlit and streamlit-aggrid

## Others:

Jupyter Notebook server, Miniconda, Pandoc, TeX Live, git, emacs, jed, nano, tzdata, and unzip

## Notes:

By default, the image runs jupyter lab on the path `/home/jovyan/work/notebooks`
