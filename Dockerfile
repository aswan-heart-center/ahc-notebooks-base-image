FROM jupyter/tensorflow-notebook:d990a62010ae

WORKDIR /home/jovyan/work

# Python packages
RUN pip install \
  plotly==5.8.0 \
  plotly-geo==1.0.0 \
  pymongo==4.1.1 \
  python-dotenv==0.19.1 \
  jupyter-contrib-nbextensions==0.5.1 \
  pylantern==0.1.6 \
  qgrid==1.3.1 \
  pandas-profiling==2.11.0 \
  SQLAlchemy==1.4.37 \
  pytest==6.2.2 \
  streamlit==1.10.0 \
  streamlit-aggrid==0.2.3 \
  simplejson==3.17.2 && \
  fix-permissions $CONDA_DIR && \
  fix-permissions /home/$NB_USER

USER root

# install plotly-orca and dependencies
RUN conda install -y -c plotly plotly-orca psutil
RUN apt update && apt-get install -y --no-install-recommends \
  wget \
  xvfb \
  libgtk2.0-0 \
  libxtst6 \
  libxss1 \
  libgconf-2-4 \
  libnss3 \
  libasound2 && \
  mkdir -p /home/orca && \
  cd /home/orca && \
  wget https://github.com/plotly/orca/releases/download/v1.2.1/orca-1.2.1-x86_64.AppImage && \
  chmod +x orca-1.2.1-x86_64.AppImage && \
  ./orca-1.2.1-x86_64.AppImage --appimage-extract && \
  printf '#!/bin/bash \nxvfb-run --auto-servernum --server-args "-screen 0 640x480x24" /home/orca/squashfs-root/app/orca "$@"' > /usr/bin/orca && \
  chmod +x /usr/bin/orca

USER jovyan

RUN mkdir /home/jovyan/work/notebooks

WORKDIR /home/jovyan/work/notebooks

CMD ["jupyter", "lab"]
